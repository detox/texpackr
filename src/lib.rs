#![feature(crate_in_paths, generators, pin)]

extern crate gen_iter;
extern crate image;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

mod patch_node;

use std::{
    collections::HashMap,
    ffi::OsStr,
    fs::{self, File},
    io::Write,
    path::{Path, PathBuf},
};

use image::{imageops, GenericImage, ImageBuffer, SubImage};

use crate::patch_node::PatchNode;

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Config {
    pub width: u32,
    pub height: u32,
    #[serde(default)]
    pub padding: u32,
    #[serde(default)]
    pub clamping: u32,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AssetData {
    pub id: String,
    pub size: (u32, u32),
    pub tiles: HashMap<String, TileData>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TileData {
    pub pos: (u32, u32),
    pub size: (u32, u32),
}

pub fn create_atlas(
    source_folder: &Path,
    asset_path: &Path,
    dest_path: &Path,
    atlas_id: &str,
    config: Config,
) {
    let path = source_folder.to_path_buf().join(atlas_id);

    let files = add_dir(asset_path, &path, atlas_id, &config);

    let total_padding = config.clamping + config.padding;

    // collect image data
    let mut image_files = Vec::new();
    for name in files {
        let img_path = asset_path.join(atlas_id).join(&name);
        let img = image::open(img_path);

        if let Ok(image) = img {
            image_files.push((name, image.dimensions()));
        }
    }

    // sort ascending by perimeter
    image_files.sort_by(|a, b| ((b.1).0 + (b.1).1).cmp(&((a.1).0 + (a.1).1)));

    // create atlas
    let mut tree = PatchNode::new(config.width, config.height);

    for (name, size) in image_files {
        tree.insert(size.0 + 2 * total_padding, size.1 + 2 * total_padding, name)
            .expect("Not enough space in atlas!");
    }

    let mut buf = ImageBuffer::new(config.width, config.height);
    let store_map = tree
        .into_iter()
        .map(|patch| {
            let pos = (patch.pos.0 + total_padding, patch.pos.1 + total_padding);
            let size = (
                patch.size.0 - 2 * total_padding,
                patch.size.1 - 2 * total_padding,
            );
            let name = patch.name;

            let img_path = path.join(&name);
            let img = image::open(&img_path)
                .expect(&format!("Couldn't open {:?}", img_path))
                .to_rgba();

            imageops::replace(&mut buf, &img, pos.0, pos.1);

            if config.clamping > 0 {
                let pixel_top_left = *buf.get_pixel(pos.0, pos.1);
                let pixel_top_right = *buf.get_pixel(pos.0 + size.0 - 1, pos.1);
                let pixel_bottom_left = *buf.get_pixel(pos.0, pos.1 + size.1 - 1);
                let pixel_bottom_right = *buf.get_pixel(pos.0 + size.0 - 1, pos.1 + size.1 - 1);

                for i in 1..=(config.clamping) {
                    // Sides
                    let row_top = SubImage::new(&mut buf, pos.0, pos.1, size.0, 1).to_image();
                    let row_bottom =
                        SubImage::new(&mut buf, pos.0, pos.1 + size.1 - 1, size.0, 1).to_image();
                    let col_left = SubImage::new(&mut buf, pos.0, pos.1, 1, size.1).to_image();
                    let col_right =
                        SubImage::new(&mut buf, pos.0 + size.0 - 1, pos.1, 1, size.1).to_image();

                    imageops::replace(&mut buf, &row_top, pos.0, pos.1 - i);
                    imageops::replace(&mut buf, &row_bottom, pos.0, pos.1 + size.1 - 1 + i);
                    imageops::replace(&mut buf, &col_left, pos.0 - i, pos.1);
                    imageops::replace(&mut buf, &col_right, pos.0 + size.1 - 1 + i, pos.1);

                    // Corners
                    for j in 1..=(config.clamping) {
                        buf.put_pixel(pos.0 - i, pos.1 - j, pixel_top_left);
                        buf.put_pixel(pos.0 + size.0 - 1 + i, pos.1 - j, pixel_top_right);
                        buf.put_pixel(pos.0 - i, pos.1 + size.1 - 1 + j, pixel_bottom_left);
                        buf.put_pixel(
                            pos.0 + size.0 - 1 + i,
                            pos.1 + size.1 - 1 + j,
                            pixel_bottom_right,
                        );
                    }
                }
            }

            (
                name.with_extension("").to_str().unwrap().to_owned(),
                TileData { pos, size },
            )
        }).collect::<HashMap<_, _>>();

    // save atlas
    buf.save(&dest_path.join(atlas_id).with_extension("png"))
        .expect("Saving atlas failed!");

    // create asset file
    let data = AssetData {
        id: atlas_id.to_owned(),
        size: (config.width, config.height),
        tiles: store_map,
    };

    // write file
    let atlas_path = dest_path.join(&atlas_id).with_extension("atlas");
    let mut f = File::create(&atlas_path).unwrap();
    let s = serde_json::to_string(&data).unwrap();
    let _ = f.write_all(s.into_bytes().as_slice()).unwrap();
}

fn add_dir(asset_path: &Path, dir: &Path, atlas_id: &str, config: &Config) -> Vec<PathBuf> {
    // rebuild when files are added or deleted
    println!("cargo:rerun-if-changed={}", dir.to_str().unwrap());

    let mut images = Vec::new();

    if dir.is_dir() {
        for entry in fs::read_dir(dir).unwrap() {
            let entry = entry.unwrap();
            let path = entry.path();

            if path.is_dir() {
                images.extend(add_dir(asset_path, &path, atlas_id, config));
            } else if path.is_file() && path.extension() == Some(OsStr::new("png")) {
                let path_prefix = asset_path.join(atlas_id);
                let path_str = path.strip_prefix(&path_prefix).unwrap();

                // rebuild when file is changed
                println!("cargo:rerun-if-changed={}", path.to_str().unwrap());

                images.push(path_str.to_path_buf());
            }
        }
    }

    images
}
