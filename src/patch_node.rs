use std::{marker::Unpin, path::PathBuf};

use gen_iter::GenIter;

#[derive(Debug)]
enum PatchNodeInner {
    Empty,
    Leaf { name: PathBuf },
    Split {
        first: Box<PatchNode>,
        second: Box<PatchNode>,
    },
}

#[derive(Debug)]
pub struct PatchNode {
    pos: (u32, u32),
    size: (u32, u32),
    inner: PatchNodeInner,
}

#[derive(Clone, Copy, Debug)]
pub struct Patch<T> {
    pub pos: (u32, u32),
    pub size: (u32, u32),
    pub name: T,
}

impl PatchNode {
    pub fn new(width: u32, height: u32) -> PatchNode {
        PatchNode::empty((0, 0), (width, height))
    }

    fn empty(pos: (u32, u32), size: (u32, u32)) -> PatchNode {
        PatchNode {
            pos,
            size,
            inner: PatchNodeInner::Empty,
        }
    }

    pub fn into_iter(self) -> Box<Iterator<Item = Patch<PathBuf>> + Unpin> {
        Box::new(GenIter(move || match self.inner {
            PatchNodeInner::Empty => {}
            PatchNodeInner::Leaf { name } => {
                yield Patch {
                    pos: self.pos,
                    size: self.size,
                    name,
                };
            }
            PatchNodeInner::Split { first, second } => {
                for patch in first.into_iter() {
                    yield patch;
                }

                for patch in second.into_iter() {
                    yield patch;
                }
            }
        }))
    }

    pub fn insert(&mut self, width: u32, height: u32, name: PathBuf) -> Result<(), PathBuf> {
        let delta_w = self.size.0 as i64 - width as i64;
        let delta_h = self.size.1 as i64 - height as i64;

        if delta_w < 0 || delta_h < 0 {
            return Err(name);
        }

        match self.inner {
            PatchNodeInner::Empty => {
                if delta_w == 0 && delta_h == 0 {
                    self.inner = PatchNodeInner::Leaf { name };
                } else if delta_w > delta_h {
                    let mut first = Box::new(PatchNode::empty(self.pos, (width, self.size.1)));

                    let second = Box::new(PatchNode::empty(
                        (self.pos.0 + width, self.pos.1),
                        (self.size.0 - width, self.size.1),
                    ));

                    // we verified at the start of the function that self.height is bigger or equal
                    // to height, so an empty PatchNode of size (width, self.height) is guaranteed
                    // to be large enough for a patch of size (width, height)
                    first.insert(width, height, name).unwrap();
                    self.inner = PatchNodeInner::Split { first, second };
                } else {
                    let mut first = Box::new(PatchNode::empty(self.pos, (self.size.0, height)));

                    let second = Box::new(PatchNode::empty(
                        (self.pos.0, self.pos.1 + height),
                        (self.size.0, self.size.1 - height),
                    ));

                    // we verified at the start of the function that self.width is bigger or equal
                    // to width, so an empty PatchNode of size (self.width, height) is guaranteed
                    // to be large enough for a patch of size (width, height)
                    first.insert(width, height, name).unwrap();
                    self.inner = PatchNodeInner::Split { first, second };
                }

                Ok(())
            }
            PatchNodeInner::Leaf { .. } => Err(name),
            PatchNodeInner::Split {
                ref mut first,
                ref mut second,
            } => first
                .insert(width, height, name)
                .or_else(|name| second.insert(width, height, name)),
        }
    }
}
